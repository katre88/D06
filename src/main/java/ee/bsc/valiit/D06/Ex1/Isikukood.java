package ee.bsc.valiit.D06.Ex1;

import java.util.Scanner;

public class Isikukood {
    public static final Scanner SISEND = new Scanner(System.in);
    public static int idCode() {
        int idCode = Integer.parseInt(SISEND.nextLine().substring(0, 3));
        int year = Integer.parseInt(Integer.toString(idCode).substring(1, 3));
        int century = Integer.parseInt(Integer.toString(idCode).substring(0, 1));
        switch (century) {
            case 3:
            case 4:
                year = year + 1900;
                break;
            case 5:
            case 6:
                year = year + 2000;
                break;
            default:
                break;
        }
        return year;

    }

    public static void main(String[] args) {
        System.out.println("Sisesta isikukood:");
        System.out.println(idCode());

    }
}
